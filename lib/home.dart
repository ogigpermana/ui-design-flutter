import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _bottomNavIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: new BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        fixedColor: Colors.cyan,
        currentIndex: _bottomNavIndex,
        onTap: (int index) {
          setState(() {
            _bottomNavIndex = index;
          });
        },
        items: [
          new BottomNavigationBarItem(
              title: new Text(""), icon: new Icon(Icons.home)),
          new BottomNavigationBarItem(
              title: new Text(""), icon: new Icon(Icons.place)),
          new BottomNavigationBarItem(
              title: new Text(""), icon: new Icon(Icons.poll)),
          new BottomNavigationBarItem(
              title: new Text(""), icon: new Icon(Icons.local_offer)),
          new BottomNavigationBarItem(
              title: new Text(""), icon: new Icon(Icons.beenhere))
        ],
      ),
      appBar: new AppBar(
        title: new Text("Home"),
        backgroundColor: Colors.cyan,
        elevation: 4.0,
        iconTheme: new IconThemeData(color: Color(0xFFFFFFFF)),
      ),
      body: MainContent(),
    );
  }
}

class MainContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: new Container(
            child: new Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    new Text(
                      "Explore",
                      style: new TextStyle(
                          fontFamily: "Roboto",
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black54),
                      textAlign: TextAlign.left,
                    )
                  ],
                ),
                new SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 5.0),
                        child: new Container(
                          height: 100.0,
                          decoration: new BoxDecoration(
                              borderRadius: new BorderRadius.circular(3.0),
                              color: Color(0xff26a69a)),
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Icon(
                                Icons.drive_eta,
                                color: Colors.white,
                              ),
                              new Text(
                                "Goto place",
                                style: new TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    new Expanded(
                      child: new Container(
                        height: 100.0,
                        child: new Column(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 2.5, right: 2.5),
                                child: new Container(
                                  decoration: new BoxDecoration(
                                      color: Color(0xff00acc1),
                                      borderRadius: BorderRadius.circular(3.0)),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 8.0),
                                        child: new Icon(
                                          Icons.place,
                                          color: Colors.white,
                                        ),
                                      ),
                                      new Text(
                                        "Place",
                                        style:
                                            new TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 2.5, right: 2.5),
                                child: new Container(
                                  decoration: new BoxDecoration(
                                      color: Color(0xff4caf50),
                                      borderRadius: BorderRadius.circular(3.0)),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 8.0),
                                        child: new Icon(
                                          Icons.home,
                                          color: Colors.white,
                                        ),
                                      ),
                                      new Text(
                                        "Home",
                                        style:
                                            new TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    new Expanded(
                      child: new Container(
                        height: 100.0,
                        child: new Column(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 2.5, bottom: 2.5),
                                child: new Container(
                                  decoration: new BoxDecoration(
                                      color: Color(0xff607b8d),
                                      borderRadius: BorderRadius.circular(3.0)),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 8.0),
                                        child: new Icon(
                                          Icons.poll,
                                          color: Colors.white,
                                        ),
                                      ),
                                      new Text(
                                        "Charts",
                                        style:
                                            new TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 2.5, left: 2.5),
                                child: new Container(
                                  decoration: new BoxDecoration(
                                      color: Color(0xfffc6a7f),
                                      borderRadius: BorderRadius.circular(3.0)),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 8.0),
                                        child: new Icon(
                                          Icons.beenhere,
                                          color: Colors.white,
                                        ),
                                      ),
                                      new Text(
                                        "Services",
                                        style:
                                            new TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                new SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text(
                        "Popular Trending",
                        style: new TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black54),
                      ),
                    ),
                    new Expanded(
                      child: new Text(
                        "See All",
                        style: new TextStyle(
                            fontSize: 12.0,
                            color: Color(0xff00bcd4),
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
                new SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 2.5),
                              child: new Container(
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    image: new DecorationImage(
                                        image: new NetworkImage(
                                            'https://i0.wp.com/www.healthline.com/hlcmsresource/images/AN_images/foods-that-cause-food-poisoning-1296x728.jpg?w=1155&h=1528'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            new Text(
                              "Food Poisoning",
                              style: new TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                    new Expanded(
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 2.5, right: 2.5),
                              child: new Container(
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    image: new DecorationImage(
                                        image: new NetworkImage(
                                            'https://www.tmb.ie/wp/wp-content/uploads/2015/08/how-to-prevent-food-poisoning.jpg'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            new Text(
                              "Food Poisoning",
                              style: new TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                    new Expanded(
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 2.5),
                              child: new Container(
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    image: new DecorationImage(
                                        image: new NetworkImage(
                                            'https://www.health.mil/-/media/Images/MHS/Photos/Grilling-safety.ashx?h=428&la=en&mw=720&w=720&hash=7599B88ECC3D6287D31AEDB8342E87180C857E3A'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            new Text(
                              "Grilling Safety",
                              style: new TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                new SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text(
                        "Featured Ads",
                        style: new TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black54),
                      ),
                    ),
                    new Expanded(
                      child: new Text(
                        "See All",
                        style: new TextStyle(
                            fontSize: 12.0,
                            color: Color(0xff00bcd4),
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
                new SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 2.5),
                              child: new Container(
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    image: new DecorationImage(
                                        image: new NetworkImage(
                                            'https://www.untoldmorsels.com/wp-content/uploads/2016/07/honfleur-market-normandy-france-1440x1080.jpg'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            new Text(
                              "Food Fruit",
                              style: new TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                    new Expanded(
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 2.5, right: 2.5),
                              child: new Container(
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    image: new DecorationImage(
                                        image: new NetworkImage(
                                            'https://www.londonperfect.com/g/photos/apartments/org_80837127-1507910834-LT-portobello-road-market-food-vegetables-london.jpg'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            new Text(
                              "Food Vegetarian",
                              style: new TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                    new Expanded(
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 2.5),
                              child: new Container(
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    image: new DecorationImage(
                                        image: new NetworkImage(
                                            'https://yourhealth.augustahealth.org/wp-content/uploads/sites/21/2016/05/Color-Me-Healthy-small-810x541.jpg'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            new Text(
                              "Vegetables",
                              style: new TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                new SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text(
                        "Latest Topic",
                        style: new TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black54),
                      ),
                    ),
                    new Expanded(
                      child: new Text(
                        "See All",
                        style: new TextStyle(
                            fontSize: 12.0,
                            color: Color(0xff00bcd4),
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
                new SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 2.5),
                              child: new Container(
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    image: new DecorationImage(
                                        image: new NetworkImage(
                                            'https://i0.wp.com/www.healthline.com/hlcmsresource/images/AN_images/foods-that-cause-food-poisoning-1296x728.jpg?w=1155&h=1528'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            new Text(
                              "Food Poisoning",
                              style: new TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                    new Expanded(
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 2.5, right: 2.5),
                              child: new Container(
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    image: new DecorationImage(
                                        image: new NetworkImage(
                                            'https://www.tmb.ie/wp/wp-content/uploads/2015/08/how-to-prevent-food-poisoning.jpg'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            new Text(
                              "Food Poisoning",
                              style: new TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                    new Expanded(
                      child: Container(
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 2.5),
                              child: new Container(
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    image: new DecorationImage(
                                        image: new NetworkImage(
                                            'https://www.health.mil/-/media/Images/MHS/Photos/Grilling-safety.ashx?h=428&la=en&mw=720&w=720&hash=7599B88ECC3D6287D31AEDB8342E87180C857E3A'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                            new Text(
                              "Grilling Safety",
                              style: new TextStyle(
                                  fontSize: 14.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
